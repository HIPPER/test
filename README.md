# RSS Parser

## Getting Started
To run the project, you need to have [Docker](https://www.docker.com/products/docker-desktop/) and [make](https://gnuwin32.sourceforge.net/packages/make.htm) utility installed. If you have them, enter the following commands: 
```bash
make build
```
```bash
make up
```
Then, open a browser and enter the URL: http://localhost:3000/.

To log in, use the login and password: admin, admin.

## Author
This project was created by Dmitry Avtenev.
