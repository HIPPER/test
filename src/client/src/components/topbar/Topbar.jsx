import { useState } from "react";
import { useHistory } from "react-router-dom";
import Input from "@mui/material/Input";
import SearchIcon from "@mui/icons-material/Search";
import "./topbar.css";

export default function Topbar() {
  const history = useHistory();
  const [open, setOpen] = useState(false);
  const handleSearchInput = () => {
    setOpen(!open);
  };

  const handleSearch = (event) => {
    if (event.key === "Enter") {
      const path = `/?search=${event.target.value}`;
      history.push(path);
      history.go(0);
    }
  };

  return (
    <div className="topbarContainer">
      <div className="topbarLeft">
        <span className="logo">Admin</span>
      </div>
      <div className="topbarRight">
        <div className="topbarIcons">
          {open ? <Input placeholder="Search" onKeyDown={handleSearch} /> : ""}
          <div className="topbarIconItem">
            <SearchIcon onClick={handleSearchInput} />
          </div>
        </div>
      </div>
    </div>
  );
}
