import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import Post from "../post/Post";
import Toolbar from "../toolbar/Toolbar";
import { getPosts, searchPosts } from "../../http/postAPI";

import "./feed.css";

const LIMIT = 5;

export default function Feed() {
  const [posts, setPosts] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalCount, setTotalCount] = useState(0);

  const location = useLocation();
  const params = new URLSearchParams(location.search);
  const sortType = params.get("sort");

  useEffect(() => {
    async function fetchData() {
      if (params.get("search")) {
        const search = params.get("search");
        const data = await searchPosts(search);

        setPosts(data);
        return;
      }
      const data = await getPosts(currentPage, LIMIT, sortType);
      setPosts(data.data);
      setTotalCount(data.totalCount);
    }

    fetchData();
  }, [currentPage]);

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const pageCount = Math.ceil(totalCount / LIMIT);

  const pagination = [];

  for (let i = 1; i <= pageCount; i++) {
    pagination.push(
      <li key={i}>
        <button onClick={() => handlePageChange(i)}>{i}</button>
      </li>
    );
  }

  return (
    <div className="feed">
      <div className="feedWrapper">
        <div className="toolbar">
          <Toolbar page={currentPage} />
        </div>
        <div className="posts">
          {posts.length ? (
            posts.map((post) => <Post key={post._id} post={post} />)
          ) : (
            <div className="warning">No posts!</div>
          )}
        </div>
        <div className="paginationbar">
          <ul className="pagination">{pagination}</ul>
        </div>
      </div>
    </div>
  );
}
