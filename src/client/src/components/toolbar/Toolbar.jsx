import { useState } from "react";
import { useHistory } from "react-router-dom";
import AddIcon from "@mui/icons-material/Add";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Filter from "../filter/Filter";
import { createPost } from "../../http/postAPI";
import "./toolbar.css";

export default function Topbar() {
  const [open, setOpen] = useState(false);
  const [title, setTitle] = useState();
  const [content, setContent] = useState();
  const history = useHistory();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleCreatePost = () => {
    if (title && content) {
      createPost(title, content);
      handleClose();
      setTitle(null);
      setContent(null);
      history.go(0);
    }
  };

  const handleChangeTitle = (event) => {
    setTitle(event.target.value);
  };

  const handleChangeContent = (event) => {
    setContent(event.target.value);
  };

  return (
    <div className="toolbarContainer">
      <div className="toolbarLeft">
        <Filter />
      </div>
      <div className="toolbarRight">
        <div className="toolbarIcons">
          <div className="toolbarIconItem">
            <Button
              color="primary"
              variant="contained"
              endIcon={<AddIcon />}
              onClick={handleClickOpen}
            >
              Create
            </Button>
            <Dialog fullWidth maxWidth="sm" open={open} onClose={handleClose}>
              <DialogTitle>Сreate a new post</DialogTitle>
              <DialogContent>
                <TextField
                  autoFocus
                  margin="dense"
                  id="title"
                  label="Title"
                  fullWidth
                  variant="standard"
                  required
                  onChange={handleChangeTitle}
                />
                <TextField
                  margin="dense"
                  id="content"
                  label="Content"
                  multiline
                  rows={6}
                  fullWidth
                  variant="standard"
                  required
                  onChange={handleChangeContent}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <Button onClick={handleCreatePost}>Create</Button>
              </DialogActions>
            </Dialog>
          </div>
        </div>
      </div>
    </div>
  );
}
