import { useState } from "react";
import { useHistory } from "react-router-dom";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { editPost, deletePost } from "../../http/postAPI";
import "./post.css";

export default function Post({ post }) {
  const dateData = new Date(post.pubDate),
    day = dateData.getDate(),
    month = dateData.getMonth() + 1,
    year = dateData.getFullYear();

  const date = [day, month, year].join("-");

  const [open, setOpen] = useState(false);
  const [_id] = useState(post._id);
  const [title, setTitle] = useState(post.title);
  const [content, setContent] = useState(post.content);
  const history = useHistory();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleEditPost = () => {
    if (title && content) {
      editPost(_id, title, content);
      handleClose();
      history.go(0);
    }
  };

  const handleDeletePost = () => {
    if (title && content) {
      deletePost(_id, title, content);
      handleClose();
      history.go(0);
    }
  };

  const handleChangeTitle = (event) => {
    setTitle(event.target.value);
  };

  const handleChangeContent = (event) => {
    setContent(event.target.value);
  };

  return (
    <div className="post">
      <div className="postWrapper">
        <div className="postCenter" onClick={handleClickOpen}>
          <div className="postText">{post.title}</div>
          <div dangerouslySetInnerHTML={{ __html: post.content }} />
          <div className="postDate">{date}</div>
        </div>

        <Dialog fullWidth maxWidth="sm" open={open} onClose={handleClose}>
          <DialogTitle>Edit post</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              id="title"
              label="Title"
              fullWidth
              variant="standard"
              required
              value={title}
              onChange={handleChangeTitle}
            />
            <TextField
              margin="dense"
              id="content"
              label="Content"
              multiline
              rows={6}
              fullWidth
              variant="standard"
              required
              value={content}
              onChange={handleChangeContent}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
            <Button onClick={handleEditPost}>Edit</Button>
            <Button
              onClick={handleDeletePost}
              color="error"
              variant="contained"
            >
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    </div>
  );
}
