import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { Context } from "./index";
import { useContext } from "react";
import Admin from "./pages/admin/Admin";
import Login from "./pages/login/Login";

const AppRouter = () => {
  const { admin } = useContext(Context);

  return (
    <Switch>
      <Route path="/" exact>
        {!admin.isAuth ? <Redirect to="/login" /> : <Admin />}
      </Route>
      <Route path="/login" exact>
        <Login />
      </Route>
    </Switch>
  );
};

export default AppRouter;
