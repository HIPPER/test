import axios from "axios";
import jwt_decode from "jwt-decode";

export const login = async (login, password) => {
  const { data } = await axios({
    method: "post",
    url: "/login",
    headers: {
      authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    params: {
      login,
      password,
    },
  });
  localStorage.setItem("token", data.token);
  return jwt_decode(data.token);
};

export const check = async () => {
  const { data } = await axios({
    method: "get",
    url: "/login/auth",
    headers: {
      authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  localStorage.setItem("token", data.token);
  return jwt_decode(data.token);
};
