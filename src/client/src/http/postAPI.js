import axios from "axios";

export const getPosts = async (currentPage, LIMIT, sortType = 0) => {
  try {
    const data = await axios.get(
      `/post?page=${currentPage}&limit=${LIMIT}&sort=${sortType}`,
      {
        headers: {
          authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    );
    return data.data;
  } catch (error) {
    console.error(error);
  }
};

export const searchPosts = async (search) => {
  try {
    const data = await axios.get(`/post?search=${search}`, {
      headers: {
        authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });
    return data.data;
  } catch (error) {
    console.error(error);
  }
};

export const createPost = async (title, content) => {
  const date = new Date();

  try {
    const status = await axios({
      method: "post",
      url: `/post?date=${date}`,
      params: {
        title,
        content,
      },
      headers: {
        authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });
    console.log(status);
  } catch (error) {
    console.error(error);
  }
};

export const editPost = async (_id, title, content) => {
  try {
    const status = await axios({
      method: "put",
      url: "/post",
      params: {
        _id,
        title,
        content,
      },
      headers: {
        authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });
    console.log(status);
  } catch (error) {
    console.error(error);
  }
};

export const deletePost = async (_id) => {
  try {
    const status = await axios({
      method: "delete",
      url: "/post",
      params: {
        _id,
      },
      headers: {
        authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });
    console.log(status);
  } catch (error) {
    console.error(error);
  }
};
