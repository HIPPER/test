import { BrowserRouter as Router } from "react-router-dom";
import AppRouter from "./routes";
import { useContext, useEffect, useState } from "react";
import CircularProgress from "@mui/material/CircularProgress";
import { Context } from "./index";
import { check } from "./http/loginAPI";

function App() {
  const { admin } = useContext(Context);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    check()
      .then((data) => {
        admin.setAdmin(true);
        admin.setIsAuth(true);
      })
      .finally(() => setLoading(false));
  }, []);

  if (loading) {
    return <CircularProgress variant="determinate" value={loading} />;
  }

  return (
    <Router>
      <AppRouter />
    </Router>
  );
}

export default App;
