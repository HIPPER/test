const Admin = require("../models/Admin");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const generateJWT = (id, login) => {
  return jwt.sign({ id, login }, process.env.SECRET_KEY, { expiresIn: "24h" });
};

class LoginController {
  async login(req, res) {
    const { login, password } = req.query;
    const admin = await Admin.findOne({ login });
    if (!admin) {
      return res.status(401).json({ message: "Invalid login" });
    }
    let comparePassword = bcrypt.compareSync(password, admin.password);
    if (!comparePassword) {
      return res.status(401).json({ message: "Invalid password" });
    }
    const token = generateJWT(admin._id, login);
    return res.json({ token });
  }

  async check(req, res, next) {
    const token = generateJWT(req.admin.id, req.admin.login);
    return res.json({ token });
  }
}

module.exports = new LoginController();
