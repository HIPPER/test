const Post = require("../models/Post");

class PostController {
  async getPosts(req, res) {
    if (req.query.search) {
      const { search } = req.query;
      const posts = await Post.find({ title: { $regex: search } });

      res.status(200).json(posts);
      return;
    }

    const { page, limit, sort } = req.query;
    const startIndex = (page - 1) * limit;

    const sortType = sort == 1 ? 1 : -1;
    const posts = {};

    try {
      posts.data = await Post.find()
        .sort([["pubDate", sortType]])
        .limit(limit)
        .skip(startIndex);
      posts.totalCount = await Post.countDocuments();

      res.status(200).json(posts);
    } catch (err) {
      console.log(err);
      res.status(500).json(err);
    }
  }

  async createPost(req, res) {
    const { title, content, date } = req.query;

    try {
      const status = await Post.create({ title, content, pubDate: date });
      res.status(200).json(status);
    } catch (err) {
      console.log(err);
      res.status(500).json(err);
    }
  }

  async editPost(req, res) {
    const { _id, title, content } = req.query;

    try {
      const status = await Post.findOneAndUpdate({ _id }, { title, content });

      res.status(200).json(status);
    } catch (error) {
      console.log(err);
      res.status(500).json(err);
    }
  }

  async deletePost(req, res) {
    const _id = req.query._id;

    try {
      const status = await Post.deleteOne({ _id });

      res.status(200).json(status);
    } catch (error) {
      console.log(err);
      res.status(500).json(err);
    }
  }
}

module.exports = new PostController();
