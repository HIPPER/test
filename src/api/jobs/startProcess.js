const rssParser = require("./rssParser");
const Admin = require("../models/Admin");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const registration = async () => {
  const login = "admin",
    password = "admin";

  const candidate = await Admin.findOne({ login });
  if (candidate) {
    console.log("Registration not required!");
    return;
  }

  const hashPassword = await bcrypt.hash(password, 2);
  await Admin.create({ login, password: hashPassword });
  console.log("Registration completed!");
};

module.exports = async () => {
  await rssParser();
  await registration();
};
