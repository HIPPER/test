const Post = require("../models/Post");
const parser = require("../lib/rss-parser");

module.exports = async () => {
  const RSS_URL = process.env.RSS_URL || "https://lifehacker.com/rss";
  const posts = await parser.fetchFeed(RSS_URL);

  try {
    posts.forEach(async (item) => {
      const existPost = await Post.findOne({ title: item.title });
      if (!existPost) {
        const post = new Post(item);
        await post.save();
      }
    });
  } catch (err) {
    console.log(err);
  }

  console.log("RSS Parser job completed successfully");
};
