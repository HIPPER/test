const Parser = require("rss-parser");
const parser = new Parser();

const fetchFeed = (exports.fetchFeed = async function (url) {
  try {
    const feed = await parser.parseURL(url);
    let items = [];

    await Promise.all(
      feed.items.map(async (currentItem) => {
        items.push(currentItem);
      })
    );

    return items;
  } catch (err) {
    throw err;
  }
});
