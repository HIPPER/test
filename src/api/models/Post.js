const mongoose = require("mongoose");

const PostSchema = new mongoose.Schema({
  creator: {
    type: String,
  },
  title: {
    type: String,
    unique: true,
  },
  link: {
    type: String,
  },
  pubDate: {
    type: Date,
  },
  content: {
    type: String,
  },
  contentSnippet: {
    type: String,
  },
});

module.exports = mongoose.model("Post", PostSchema);
