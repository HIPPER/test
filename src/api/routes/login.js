const router = require("express").Router();
const loginController = require("../controllers/loginController");
const authMiddleware = require("../middleware/authMiddleware");

router.get("/auth", authMiddleware, loginController.check);
router.post("/", loginController.login);

module.exports = router;
