const router = require("express").Router();
const postController = require("../controllers/postController");
const authMiddleware = require("../middleware/authMiddleware");

router.get("/", postController.getPosts);

router.post("/", authMiddleware, postController.createPost);

router.put("/", authMiddleware, postController.editPost);

router.delete("/", authMiddleware, postController.deletePost);

module.exports = router;
