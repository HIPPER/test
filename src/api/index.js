require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");

const scheduler = require("./scheduler");
const startProcess = require("./jobs/startProcess");

const postRoute = require("./routes/posts");
const loginRoute = require("./routes/login");

const app = express();

const PORT = process.env.PORT || 3001;
const MONGO_URL = process.env.MONGO_URL || "mongodb://mongo:27017/rss";

async function databaseStart() {
  try {
    await mongoose.connect(MONGO_URL).then(console.log("Connected to MongoDB"));
    await startProcess();
  } catch (e) {
    console.log("Server Error", e.message);
    process.exit(1);
  }
}

databaseStart();
scheduler.initCrons();

app.use("/api/post", postRoute);
app.use("/api/login", loginRoute);

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
