const cron = require("node-cron");

module.exports = {
  initCrons: () => {
    try {
      cron.schedule("10 * * * *", async () => {
        const rssParserJob = require("./jobs/rssParser");
        await rssParserJob();
      });
    } catch (error) {
      console.log("Cron Error", error.message);
    }
  },
};
